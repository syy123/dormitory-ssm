package com.southwind.service;

import com.southwind.entity.Moveout;
import com.southwind.entity.Student;

import java.util.List;

public interface StudentServie {
    // 查询所有学生信息
    public List<Student> list();
    // 根据关键字和值查询学生信息
    public List<Student> search(String key,String value);
    // 添加学生信息
    public void save(Student student);
    // 更新学生信息
    public void update(Student student);
    // 删除学生信息
    public void delete(Student student);
    // 查询所有已入住的学生信息
    public List<Student> moveoutList();
    // 根据关键字和值查询已入住的学生信息
    public List<Student> searchForMoveoutList(String key,String value);
    // 学生迁出操作
    public void moveout(Moveout moveout);
    // 根据宿舍id查询该宿舍内的所有学生信息
    public List<Student> findByDormitoryId(Integer dormitoryId);
}
