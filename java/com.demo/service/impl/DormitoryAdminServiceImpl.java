package com.southwind.service.impl;

import com.southwind.entity.DormitoryAdmin;
import com.southwind.mapper.DormitoryAdminMapper;
import com.southwind.service.DormitoryAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


//这段代码是一个实现了`DormitoryAdminService`接口的`DormitoryAdminServiceImpl`类。它使用了`@Service`注解来标识该类为一个服务组件。
//
//        在该类中，注入了`DormitoryAdminMapper`对象，用于与数据库进行数据访问操作。
@Service
public class DormitoryAdminServiceImpl implements DormitoryAdminService {


    @Autowired
    private DormitoryAdminMapper dormitoryAdminMapper;
    //    `List<DormitoryAdmin> list()`: 该方法通过调用`dormitoryAdminMapper`的`list()`方法，返回所有宿舍管理员的列表。


    @Override
    public List<DormitoryAdmin> list() {
        return this.dormitoryAdminMapper.list();
    }
    //    `List<DormitoryAdmin> search(String key, String value)`: 该方法根据给定的`key`和`value`参数进行搜索操作。


    @Override
    public List<DormitoryAdmin> search(String key, String value) {
        if(key.equals("")) return this.dormitoryAdminMapper.list();
        List<DormitoryAdmin> list = null;
        switch (key){
            case "username":
                list = this.dormitoryAdminMapper.searchByUsername(value);
                break;
            case "name":
                list = this.dormitoryAdminMapper.searchByName(value);
                break;
            case "telephone":
                list = this.dormitoryAdminMapper.searchByTelephone(value);
                break;
        }
        return list;
    }

    //    `void save(DormitoryAdmin dormitoryAdmin)`: 该方法通过调用`dormitoryAdminMapper`的`save()`方法，将传入的`dormitoryAdmin`对象保存（添加）到数据库中。


    @Override
    public void save(DormitoryAdmin dormitoryAdmin) {
        try {
            this.dormitoryAdminMapper.save(dormitoryAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    `void delete(Integer id)`: 该方法通过调用`dormitoryAdminMapper`的`delete()`方法，删除指定ID的宿舍管理员
    @Override
    public void delete(Integer id) {
        try {
            this.dormitoryAdminMapper.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    `void update(DormitoryAdmin dormitoryAdmin)`: 该方法通过调用`dormitoryAdminMapper`的`update()`方法，更新指定宿舍管理员的信息
    @Override
    public void update(DormitoryAdmin dormitoryAdmin) {
        try {
            this.dormitoryAdminMapper.update(dormitoryAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
