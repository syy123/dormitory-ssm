package com.southwind.service.impl;

import com.southwind.entity.Building;
import com.southwind.mapper.BuildingMapper;
import com.southwind.mapper.DormitoryMapper;
import com.southwind.mapper.StudentMapper;
import com.southwind.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingServiceImpl implements BuildingService {

    @Autowired
    private BuildingMapper buildingMapper;
    @Autowired
    private DormitoryMapper dormitoryMapper;
    @Autowired
    private StudentMapper studentMapper;

    /**
     * 获取所有楼栋信息
     */
    @Override
    public List<Building> list() {
        return this.buildingMapper.list();
    }

    /**
     * 根据关键字搜索楼栋信息
     */
    @Override
    public List<Building> search(String key, String value) {
        if (value.equals("")) {
            return this.buildingMapper.list();
        }
        List<Building> list = null;
        switch (key) {
            case "name":
                list = this.buildingMapper.searchByName(value);
                break;
            case "introduction":
                list = this.buildingMapper.searchByIntroduction(value);
                break;
        }
        return list;
    }

    /**
     * 保存楼栋信息
     */
    @Override
    public void save(Building building) {
        try {
            this.buildingMapper.save(building);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新楼栋信息
     */
    @Override
    public void update(Building building) {
        try {
            this.buildingMapper.update(building);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除楼栋信息
     */
    @Override
    public void delete(Integer id) {
        try {
            // 获取楼栋包含的所有宿舍ID
            List<Integer> dormitoryIdList = this.dormitoryMapper.findDormitoryIdByBuildingId(id);

            // 遍历宿舍ID列表
            for (Integer dormitoryId : dormitoryIdList) {
                // 获取宿舍包含的所有学生ID
                List<Integer> studentIdList = this.studentMapper.findStudentIdByDormitoryId(dormitoryId);

                // 遍历学生ID列表
                for (Integer studentId : studentIdList) {
                    // 获取可用的宿舍ID
                    Integer availableDormitoryId = this.dormitoryMapper.findAvailableDormitoryId();

                    // 将学生调换到可用的宿舍
                    this.studentMapper.resetDormitoryId(studentId, availableDormitoryId);

                    // 减少可用宿舍的可用床位数
                    this.dormitoryMapper.subAvailable(availableDormitoryId);
                }

                // 删除宿舍
                this.dormitoryMapper.delete(dormitoryId);
            }

            // 删除楼栋
            this.buildingMapper.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
