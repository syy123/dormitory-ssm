package com.southwind.controller;

import com.southwind.entity.Student;
import com.southwind.service.DormitoryService;
import com.southwind.service.StudentServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    // 自动注入StudentService和DormitoryService
    @Autowired
    private StudentServie studentServie;
    @Autowired
    private DormitoryService dormitoryService;

    // 显示所有学生信息
    @GetMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("studentmanager"); // 返回视图名称
        modelAndView.addObject("list", this.studentServie.list()); // 添加学生列表
        modelAndView.addObject("dormitoryList", this.dormitoryService.availableList()); // 添加可用的宿舍列表
        return modelAndView;
    }

    // 搜索学生信息
    @PostMapping("/search")
    public ModelAndView search(String key,String value){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("studentmanager"); // 返回视图名称
        modelAndView.addObject("list", this.studentServie.search(key, value)); // 添加符合搜索条件的学生列表
        modelAndView.addObject("dormitoryList", this.dormitoryService.availableList()); // 添加可用的宿舍列表
        return modelAndView;
    }

    // 添加学生信息
    @PostMapping("/save")
    public String save(Student student){
        this.studentServie.save(student); // 保存学生信息
        return "redirect:/student/list"; // 重定向到显示学生列表的页面
    }

    // 更新学生信息
    @PostMapping("/update")
    public String update(Student student){
        this.studentServie.update(student); // 更新学生信息
        return "redirect:/student/list"; // 重定向到显示学生列表的页面
    }

    // 删除学生信息
    @PostMapping("/delete")
    public String delete(Student student){
        this.studentServie.delete(student); // 删除学生信息
        return "redirect:/student/list"; // 重定向到显示学生列表的页面
    }

    // 根据宿舍ID查找学生信息
    @PostMapping("/findByDormitoryId")
    @ResponseBody
    public List<Student> findByDormitoryId(Integer dormitoryId){
        return this.studentServie.findByDormitoryId(dormitoryId); // 返回符合宿舍ID的学生列表
    }
}

